package com.jwtwithdb.api.models;

import javax.persistence.*;

@Entity
@Table(	name = "questions" )
public class Question {
	private Long id;
	private String title;
	private Long userId;
	private boolean status;
	private boolean statusAnswer = false;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isStatusAnswer() {
		return statusAnswer;
	}

	public void setStatusAnswer(boolean statusAnswer) {
		this.statusAnswer = statusAnswer;
	}
}
