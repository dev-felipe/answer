package com.jwtwithdb.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jwtwithdb.api.models.Question;
import com.jwtwithdb.api.repository.QuestionRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/question")
public class QuestionController {
	
	@Autowired
	private QuestionRepository questionRepository;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    public boolean insertNewPeople(@RequestBody Question question) {
        try {
        	question = questionRepository.save(question);
            return true;
        } catch (Exception e){
        	return false;
        }
    }
}
