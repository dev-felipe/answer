import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
   private roles: string[];
    isLoggedIn = false;
    showAdminBoard = false;
    showModeratorBoard = false;
    showUserBoard = false;
    username: string;
    linkGerado: boolean = false;

  constructor(private tokenStorageService: TokenStorageService) { }

  ngOnInit() {
  this.isLoggedIn = !!this.tokenStorageService.getToken();

      if (this.isLoggedIn) {
        const user = this.tokenStorageService.getUser();
        this.roles = user.roles;

        this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
        this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
        this.showUserBoard = this.roles.includes('ROLE_USER');

        this.username = user.username;

        
      role: [user.role]
      }
  }

  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();
  }

  gerarLink(user: String){
      
  }

}
