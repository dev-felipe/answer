import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { TokenStorageService } from '../_services/token-storage.service'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit {
  content = [];
  private roles: string[];
  isLoggedIn = false;
  showUserBoard = false;
  username: string;

  constructor(private userService: UserService, private tokenStorageService: TokenStorageService, private router: Router) { }

  ngOnInit() {
    debugger;
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    this.showUserBoard = this.roles.includes('ROLE_USER');
    role: [user.role]
    if(!this.showUserBoard){
      this.router.navigate(['/home']);
    }
    this.buscaUsuario();

    
  }

  buscaUsuario() {
    debugger;
    const user = this.tokenStorageService.getUser();
    this.username = user.username;
    this.userService.getUserBoard(this.username).subscribe(
      data => {
        this.content = data as [];
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }
}
